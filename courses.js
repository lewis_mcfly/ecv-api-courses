module.exports = [
  {
    code: "session-1",
    title: "Session 1 - Theory"
  },
  {
    code: "session-2",
    title: "Session 2 - First steps with Laravel"
  },
  {
    code: "session-3",
    title: "Session 3 - Laravel: API-oriented"
  },
  {
    code: "session-4",
    title: "Session 4 - Practical Work"
  },
  {
    code: "session-5",
    title: "Session 5 - First steps with Vue.js"
  },
  {
    code: "session-6",
    title: "Session 6 - Consume your Laravel-based API with Vue.js"
  }
];
